# A simple cluster with Terraform

This is shaping up to be either a [Nomad](https://www.nomadproject.io/) or
an [Hazelcast](https://hazelcast.org/) cluster on AWS.

## Create the cluster

First check the plan:
```
terraform plan -var-file=private.tfvars
```

Then execute it:
```
terraform apply -var-file=private.tfvars
```

## Variables

These are the required inputs:

* **name**: a common prefix added to the name of the resources.
* **region**: the AWS region where the resources will be created.
* **vpc_cidr**: the CIDR of the VPC.
* **ssh_cidr**: source CIDR used for SSH access to the bastion host.
* **tags**: a map of tags to add to all resources.

## Useful examples

* https://github.com/kz8s/tack
* https://github.com/xuwang/kube-aws-terraform
* https://github.com/xuwang/aws-terraform
* https://www.hashicorp.com/blog/auto-bootstrapping-a-nomad-cluster

For Hazelcast:

* https://github.com/hazelcast/hazelcast-aws
* https://github.com/Codelab27/ansible-hazelcast
* https://gist.github.com/nscavell/a7727f16c402a1e6e4fa
* https://groups.google.com/forum/#!msg/vertx/MvKcz_aTaWM/QM6CfllX9HQJ
* https://groups.google.com/forum/#!topic/vertx/zC8lB00PTTU
* https://stackoverflow.com/questions/30103306/hazelcast-in-multinode-docker-environments-with-tcpip
