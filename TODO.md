# TODO

- [x] Assign tag Project to all resources
- [x] Assign names to subnets
- [x] Assign name to Main routing table
- [x] Enable DNS in public subnets
- [x] Create security group for incoming traffic
- [x] Improve variables with a tfvars file
- [x] Add NAT Gateway instances
- [ ] Modify security groups to allow access from within the VPC
- [x] Add Bastion host in an AutoScalig Group
- [x] Activate AWS logging in bastion host
- [x] Assign fixed EIP to bastion host. Must be done manually.
- [ ] Improve configuration of awslogs
- [ ] Improve configuration of bastion host

## Epics

- [ ] Create an AMI with Nomad and Consul already installed (Ubuntu)
    * Probably use ansible `ec2_ami`
- [ ] Create a customized Hazelcast Docker image
    * Smaller base image (e.g. Alpine)
    * Config file for AWS
    * Maybe better configuration of the JVM
    * Automatic -Xmx according to the instance type (use a simple csv file?)
