/*
 * AWS Bastion Host Terraform module.
 *
 * Copyright (c) 2017 Andrea Cisternino
 * Licensed under the Apache License, version 2.0.
 */

##---- AutoScaling Group ----------------------------------

resource "aws_autoscaling_group" "bastion" {
  name                 = "${var.name}"
  launch_configuration = "${aws_launch_configuration.bastion.name}"

  vpc_zone_identifier = ["${var.subnets}"]

  desired_capacity          = "1"
  min_size                  = "1"
  max_size                  = "1"
  health_check_grace_period = "120"
  health_check_type         = "EC2"

  tag {
    key                 = "Name"
    value               = "${var.name}-bastion-host"
    propagate_at_launch = true
  }

  tag {
    key                 = "built-with"
    value               = "terraform"
    propagate_at_launch = true
  }

  tag {
    key                 = "project"
    value               = "terraform-cluster"
    propagate_at_launch = true
  }

  tag {
    key                 = "creator"
    value               = "anci"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}

##---- Launch configuration -------------------------------

resource "aws_launch_configuration" "bastion" {
  name          = "${var.name}-launch-config"
  image_id      = "${var.ami}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"

  depends_on = ["aws_eip.bastion_ip"]

  # Remove when association to EIP works
  associate_public_ip_address = true

  iam_instance_profile = "${aws_iam_instance_profile.bastion.id}"
  security_groups      = ["${aws_security_group.bastion.id}"]

  user_data = "${data.template_cloudinit_config.config.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

##---- Elastic IP -----------------------------------------

# Public IP for the bastion host
resource "aws_eip" "bastion_ip" {
  vpc = true
}
