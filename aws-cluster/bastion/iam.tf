/*
 * AWS Bastion Host Terraform module.
 *
 * Copyright (c) 2017 Andrea Cisternino
 * Licensed under the Apache License, version 2.0.
 */

##---- Instance role --------------------------------------

resource "aws_iam_role" "bastion" {
  name        = "bastion-role"
  description = "Instance role for the Bastion Host"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "bastion" {
  name = "bastion-profile"
  role = "${aws_iam_role.bastion.name}"
}

##---- Policies -------------------------------------------

resource "aws_iam_role_policy" "awslogs" {
  name = "awslogs-policy"
  role = "${aws_iam_role.bastion.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "eip" {
  name = "eip-policy"
  role = "${aws_iam_role.bastion.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeAddresses",
        "ec2:AllocateAddress",
        "ec2:DescribeInstances",
        "ec2:AssociateAddress",
        "ec2:DisassociateAddress"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}
