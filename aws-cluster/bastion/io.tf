/*
 * AWS Bastion Host Terraform module.
 *
 * Copyright (c) 2017 Andrea Cisternino
 * Licensed under the Apache License, version 2.0.
 */

##---- Data -----------------------------------------------

data "template_file" "cloud_init" {
  template = "${file("${path.module}/scripts/cloud-init.template")}"

  vars {
    region = "${var.region}"
  }
}

data "template_file" "bootstrap" {
  template = "${file("${path.module}/scripts/bootstrap.sh")}"

  vars {
    elastic_ip = "${join(",", aws_eip.bastion_ip.*.id)}"
    region     = "${var.region}"
  }
}

data "template_cloudinit_config" "config" {
  gzip          = true
  base64_encode = true

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.bootstrap.rendered}"
  }

  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.cloud_init.rendered}"
  }
}

##---- Inputs ---------------------------------------------

variable "region" {
  description = "The AWS region to create things in"
}

variable "name" {
  description = "Name to be used on all the resources as identifier"
}

variable "vpc_id" {
  description = "ID of the main VPC"
}

variable "ssh_cidr" {
  description = "Source CIDR used for SSH access"
}

variable "subnets" {
  description = "IDs of subnets used by the AutoScaling Group"
  type        = "list"
}

variable "instance_type" {
  description = "EC2 instance type for the bastion host instances"
  default     = "t2.micro"
}

variable "ami" {}

variable "key_name" {
  description = "Name of the SSH key pair to authorize"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  default     = {}
}

##---- Outputs --------------------------------------------

output "security_group_id" {
  value = "${aws_security_group.bastion.id}"
}

output "public_ip" {
  value = "${aws_eip.bastion_ip.public_ip}"
}
