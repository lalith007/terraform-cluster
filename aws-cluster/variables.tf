/*
 * Nomad cluster with Terraform.
 *
 * Copyright (c) 2017 Andrea Cisternino
 * Licensed under the Apache License, version 2.0.
 */

variable "name" {
  description = "A common name prefix for the resources."
}

variable "aws_region" {
  description = "The AWS region to create things in."
}

variable "vpc_cidr" {
  description = "CIDR of the main VPC"
}

variable "ssh_cidr" {
  description = "Source CIDR used for SSH access"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = "map"
}
