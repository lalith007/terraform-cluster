/*
 * AWS VPC Terraform module.
 *
 * Copyright (c) 2017 Andrea Cisternino
 * Licensed under the Apache License, version 2.0.
 */

##---- Data -----------------------------------------------

data "aws_availability_zones" "all" {}

##---- Inputs ---------------------------------------------

variable "name" {
  description = "Name to be used on all the resources as identifier"
}

variable "cidr" {
  description = "CIDR of the main VPC"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  default     = {}
}

##---- Outputs --------------------------------------------

output "vpc_id" {
  value = "${aws_vpc.main.id}"
}

output "igw_id" {
  value = "${aws_internet_gateway.igw.id}"
}

output "private_subnets" {
  value = ["${aws_subnet.private.*.id}"]
}

output "public_subnets" {
  value = ["${aws_subnet.public.*.id}"]
}
