/*
 * Nomad cluster with Terraform.
 *
 * Copyright (c) 2017 Andrea Cisternino
 * Licensed under the MIT license.
 */

provider "aws" {
  region = "${var.aws_region}"
}

##---- Key Pairs ------------------------------------------

# We use the standard SSH key
resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

##---- EC2 Instances --------------------------------------

# Amazon Linux AMIs
data "aws_ami_ids" "amazon_linux" {
  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-hvm-2017*gp2"]

    # for Ubuntu: "ubuntu/images/hvm-ssd/ubuntu-xenial*"
  }
}

resource "aws_instance" "web" {
  count = 1

  ami             = "${data.aws_ami_ids.amazon_linux.ids[0]}"
  instance_type   = "t2.micro"
  key_name        = "${aws_key_pair.deployer.key_name}"
  subnet_id       = "${aws_subnet.public.0.id}"
  security_groups = ["${aws_security_group.ssh.id}", "${aws_security_group.internal.id}"]
  depends_on      = ["aws_internet_gateway.igw"]

  tags = "${merge(var.tags, map("Name", "HelloWorld"))}"
}

#---- Security Groups -------------------------------------

# SSH access from outside world
resource "aws_security_group" "ssh" {
  name        = "ssh"
  description = "SSH access open to my IP"
  vpc_id      = "${aws_vpc.nomad.id}"

  # SSH access from my IP range
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  # Outgoing access open to anywhere
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.tags, map("Name", "Nomad Open SG"))}"
}

# Full access from inside the VPC
resource "aws_security_group" "internal" {
  name        = "internal"
  description = "Open access inside the VPC"
  vpc_id      = "${aws_vpc.nomad.id}"

  # Open access from inside the VPC
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  # Outgoing access open to anywhere
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.tags, map("Name", "Nomad Internal SG"))}"
}
