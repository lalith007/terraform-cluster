/*
 * Nomad cluster with Terraform.
 *
 * VPC configuration.
 *
 * Copyright (c) 2017 Andrea Cisternino
 * Licensed under the MIT license.
 */

data "aws_availability_zones" "available" {}

##---- VPC ------------------------------------------------

resource "aws_vpc" "nomad" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_hostnames = true

  tags = "${merge(var.tags, map("Name", "Nomad VPC"))}"
}

##---- Internet Gateway -----------------------------------

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.nomad.id}"

  tags = "${merge(var.tags, map("Name", "Nomad IGW"))}"
}

##---- NAT Gateways ---------------------------------------

resource "aws_nat_gateway" "nat_gw" {
  count = "${var.az_count}"

  subnet_id     = "${element(aws_subnet.public.*.id, count.index)}"
  allocation_id = "${element(aws_eip.nat_ip.*.id, count.index)}"
}

##---- Routing Tables -------------------------------------

# Give a name to the default routing table that comes with the VPC
resource "aws_default_route_table" "default" {
  default_route_table_id = "${aws_vpc.nomad.default_route_table_id}"

  tags = "${merge(var.tags, map("Name", "Nomad Main RT"))}"
}

# Routing table for routing traffic between the public subnet and the IGW.
resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.nomad.id}"

  # The default route, mapping the VPC's CIDR block to "local", is created
  # implicitly and cannot be specified.

  route {
    # This route catches all remaining outgoing traffic and sends it
    # directly to the IGW.
    cidr_block = "0.0.0.0/0"

    gateway_id = "${aws_internet_gateway.igw.id}"
  }
  tags = "${merge(var.tags, map("Name", "Nomad Public RT"))}"
}

##---- Subnets --------------------------------------------

# Public IPs for the subnets' NATs
resource "aws_eip" "nat_ip" {
  count = "${var.az_count}"

  vpc = true

  lifecycle {
    create_before_destroy = true
  }
}

# One public subnet per AZ
resource "aws_subnet" "public" {
  count = "${var.az_count}"

  vpc_id                  = "${aws_vpc.nomad.id}"
  availability_zone       = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block              = "${cidrsubnet(aws_vpc.nomad.cidr_block, 4, count.index)}"
  map_public_ip_on_launch = true

  tags = "${merge(var.tags, map("Name", "Nomad Public Subnet ${count.index}"))}"
}

# Attach routing tables to the public subnet
resource "aws_route_table_association" "rta" {
  count = "${var.az_count}"

  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}
