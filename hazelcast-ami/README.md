# Hazelcast AMI with Ansible

This directory contains an Ansible playbook that creates an AWS AMI
for the Hazelcast Server.

## Requisites

- Ansible is installed and has access to your Secret and Access key (via EC2 role or environment variable.)
- SSH agent is running and a valid SSH key is loaded. (You can check with `ssh-add -L`.)

When executing remote plays on EC2 instances Ansible needs to connect using a
valid SSH key.

The existing key name (as visible in the EC2 console) must be provided to the
playbook using the `keypair` variable name.

```
ansible-playbook playbook.yml --extra-vars "keypair=ec2_keypair_name"
```

The corresponding `pem` file must be available to ssh and needs to be preloaded
in the key agent:

```
ssh-add /path/to/ec2_keypair_name.pem
```
