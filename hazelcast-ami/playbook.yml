---
# Hazelcast server AMI with Ansible
#
# Copyright (c) 2017 Andrea Cisternino
# Licensed under the Apache License, version 2.0.

# Local play that creates an EC2 instance and installs Hazelcast
# as a server.

- name: Provision an EC2 instance for building an Hazelcast AMI
  hosts: localhost
  connection: local
  gather_facts: false

  vars:
    aws_region: eu-west-1
    subnet:     subnet-dc5edd85
    image_id:   ami-d7b9a2b1
    type:       t2.small

  tasks:

    - name: Create required security group
      ec2_group:
        name:               open-ssh-hazelcast-build
        description:        Incoming SSH for Hazelcast build instance
        region:             "{{ aws_region }}"
        purge_rules_egress: false
        rules:
          # SSH
          - proto:     tcp
            from_port: 22
            to_port:   22
            cidr_ip:   0.0.0.0/0
          # ping
          - proto:     icmp
            from_port: 8
            to_port:   -1
            cidr_ip:   0.0.0.0/0
      register: secgrp

    - name: Tag security group
      ec2_tag:
        resource: "{{ secgrp.group_id }}"
        region: "{{ aws_region }}"
        tags:
          Name: Hazelcast AMI builder
          role: ami-builder

    - name: Launch instance
      ec2:
        region:           "{{ aws_region }}"
        key_name:         "{{ keypair }}"
        vpc_subnet_id:    "{{ subnet }}"
        instance_type:    "{{ type }}"
        image:            "{{ image_id }}"
        assign_public_ip: true
        group_id:         "{{ secgrp.group_id }}"
        wait:             true
        instance_tags:
          Name: Hazelcast AMI build
          role: ami-builder
      register: ec2instances

    - name: Waiting for SSH to start
      wait_for:
        host:  "{{ ec2instances.instances.0.public_ip }}"
        port:  22
        delay: 30
        state: started
        sleep: 5

    - name: Add instance to group
      add_host:
        name: "{{ ec2instances.instances.0.public_ip }}"
        group: ec2

#--------------------------------------------------------------------
# Remote play that configures the created instance

- name: Configures the Hazelcast Server AMI
  hosts: ec2
  remote_user: ec2-user
  become: true

  roles:

    - update
    - hazelcast
